library(tidyverse)

n <- 10 # nombre de cookies

set.seed(3) # pour la reproductibilité du jeu de données
d0 <- rnorm(n, 40, 6) # diamètre initial

jeu <- tibble(
  Cookie = factor(rep(1:n, 2)), 
  Temps = rep(c("T0", "Tf"), each = n), 
  Diametre = round(c(d0, d0 + rnorm(n, 20, 10)))
)

jeu

jeu %>% 
  group_by(Temps) %>% 
  summarise(
    Diam_Moyen = mean(Diametre), 
    Diam_EcartType = sd(Diametre)
  )

base <- ggplot(jeu) +
  # définir le rôle des variables
  aes(x = Temps, y = Diametre, colour = Temps, fill = Temps) + 
  # modifier les titres des axes
  labs(x = "", y = "Diametre - mm") + 
  # l'axe y commence par 0 pour une représentation plus réaliste
  coord_cartesian(ylim = c(0, max(jeu$Diametre) + 5), default = T) + 
  # modifie les couleurs manuellement pour colour et fill en même temps
  scale_colour_manual(
    aesthetics = c("colour", "fill")
    , values = c(T0 = "#e69f02", Tf = "#a65c34")
  ) +
  # légende inutile ici
  theme(legend.position = "none") 

base + 
  geom_point(size = 2)

jeu %>% filter(Cookie == 1)

base +
  # nouveau rôle (aes) défini afin de tracer une ligne par Cookie (group)
  geom_line(aes(group = Cookie) 
            , colour = "grey50" # couleur (unique) des lignes
  ) +
  # ordre des commandes important
  # pour que les points viennent par-dessus les lignes
  geom_point(size = 2) 

base +
  # création de nouveaux rôles (aes)
  geom_freqpoly(aes(x = Diametre, colour = Temps)
                # les rôles définis dans base ne sont pas pris en compte
                , inherit.aes = F
                # largeur des classes
                , binwidth = 4
                # épaisseur du trait
                , size = 1
  ) +
  # retrouve le système de coordonnées par défaut
  coord_cartesian() 


base +
  geom_density(aes(x = Diametre, colour = Temps) 
               , inherit.aes = F 
               , size = 1
               , adjust = 1.5 # paramètre de lissage
  ) +
  coord_cartesian() 


base +
  geom_boxplot(width = 0.3 # largeur des boîtes
               , size = 1
               , colour = "grey20" # couleur (unique) des contours
  ) +
  # ajout des tailles d'échantillonage
  annotate("text", x = 1:2, y = 75, label = paste("n =", n), family = "xkcd")


base +
  # ajout des bâtons
  geom_bar(stat = "summary" # personnalise la hauteur des bâtons
           , fun = "mean" # hauteur des bâtons = moyenne des observations
           , width = 0.3 # largeur des bâtons
           , colour = "grey20" # couleur des bâtons
           , size = 1 # largeur du trait
  ) +
  # ajout des barres d'erreur
  geom_linerange(stat = "summary" # personnalise le calcul des barres d'erreur
                 # fonction qui renvoie la moyenne 
                 # et l'intervalle de confiance de la moyenne 
                 # à 95% selon une distribution de Student 
                 # à n-1 degrés de liberté
                 , fun.data = ~ mean_se(.x, mult = qt(0.975, length(.x) - 1))
                 , colour = "grey20"
                 , size = 1 
  ) +
  # ajout des tailles d'échantillonage
  annotate("text", x = 1:2, y = 75, label = paste("n =", n), family = "xkcd")

base +
  geom_jitter(size = 1.5, width = 0.07, height = 0) +
  # ajout des barres d'erreur, 
  # pointrange rajoute un point pour indiquer la position de la moyenne
  geom_pointrange(stat = "summary" 
                  , fun.data = ~ mean_se(.x, mult = qt(0.975, length(.x) - 1))
                  # décale les barres des observations
                  # pour éviter le chevauchement
                  , position = position_nudge(x = 0.1)
                  , colour = "grey20"
                  , size = 1
                  , fatten = 2 # règle la taille du point central
  )

base +
  geom_jitter(size = 1.5, width = 0.07, height = 0) +
  # ajout des barres d'erreur, 
  # pointrange rajoute un point pour indiquer la position de la moyenne
  geom_pointrange(stat = "summary" 
                  , fun.data = mean_cl_boot
                  # décale les barres des observations
                  # pour éviter le chevauchement
                  , position = position_nudge(x = 0.1)
                  , colour = "grey20"
                  , size = 1
                  , fatten = 2 # règle la taille du point central
  )


