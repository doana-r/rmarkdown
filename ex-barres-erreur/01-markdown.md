---
title: Comment intégrer des barres d'erreur dans les graphiques avec **ggplot2**
author: "Anna Doizy"
date: "2020-04-14"
---


## Introduction

L'objectif de ce document est de vous montrer comment vous familiariser avec les **barres d'erreur dans les graphiques** en utilisant `ggplot2`.
Il ne s'agirait pas d'entraîner une mauvaise interprétation de vos données suite à de malencontreux intervalles.
Si vous confondez écart-type, erreur-type, écart inter-quartile, intervalle de confiance... 
L'enjeu est seulement de vous rafraîchir la mémoire et non de vous introduire ces notions en toute rigueur.  


**Attention !** Dans un souci de minimalisme éhonté, ceci n'est pas un tutoriel de `R`, de `dplyr` ni encore de `ggplot2`. 
Pour en savoir plus sur ces sujets passionnants (oui, sincèrement), allez voir [R for Data Science](https://r4ds.had.co.nz/), disponible gratuitement en ligne.  
Certaines notions de statistiques descriptives sont présentées très succinctement ci-après et de manière beaucoup plus précise dans ce document un peu plus long, mais beaucoup plus rigolo : [Statistiques pour statophobes](https://perso.univ-rennes1.fr/denis.poinsot/Statistiques_%20pour_statophobes/STATISTIQUES%20POUR%20STATOPHOBES.pdf).  



## Confection ~~des cookies~~ du jeu de données

Voici comment simuler un jeu de données montrant l'évolution de la taille des cookies que je viens de faire. 
Il y en a(vait) `n = 10`. 
Leur diamètre a été mesuré avant (`T0`) et après (`Tf`) leur cuisson. 
Pour information, je les ai tous mangés, ils étaient très bons.


Nous souhaitons (ardemment) comparer le diamètre des cookies entre le moment où ils ont été déposé sur la plaque et leur sortie du four. 
En effet, j'ai remarqué qu'ils avaient tendance à s'aplatir pendant la cuisson.

![](img/cookies-arrows.png)

Nous pouvons résumer les données ainsi :


Mais je trouve qu'un tableau est souvent moins parlant qu'un graphique, alors... Allons-y !  


## Observation des données

Ici, je pose la base des graphiques qui suivront. 
Cela évitera ainsi les redondances de code.
Les deux premières lignes (`ggplot` et `aes`) sont essentielles, le reste est totalement optionnel.



Tout d'abord, observons nos données brutes :


**Attention !** Les données sont appariées, c'est-à-dire que les cookies mesurés sont les mêmes entre `T0` et `Tf`.
Autrement dit, les deux lignes suivantes correspondant aux mesures effectuées sur le premier cookie ne sont pas indépendantes.


Par la suite, nous ne prendrons pas cela en compte. Pour comparer les moyennes à `T0` et `Tf` en toute rigueur , il faudrait faire un test apparié et/ou un modèle linéaire mixte en mettant `Cookie` en variable aléatoire. 
Mais ce n'est pas le sujet ici...

Voilà un exemple de représentation de la non-indépendance de ces données :


## Description des données

Pour observer la distribution des données, il est possible de construire un histogramme ou de tracer les densités :



Plus souvent, on a envie de faire un diagramme en boîtes (*boxplot*) ou encore un diagramme en bâtons (*barplot*) :




> **Remarque :** Le calcul des intervalles de confiance des moyennes correspond à 
$$ IC(95\%) = \mu \pm dispersion \times quantile(95\%)$$
avec $\mu$ la moyenne des observations. 
>
> L'indicateur de dispersion est l'erreur-type $se = \frac{sd}{\sqrt(n)}$ (nous nous intéressons à la dispersion de la moyenne), avec $sd$ l'écart-type et $n$ le nombre d'observations.  
Les mesures sont continues, supposées normalement distribuées et $n < 30$, donc le choix de la distribution des données se porte sur une loi de Student à $n-1 = 9$ degrés de liberté.
>
> Ainsi, en notant $qt$ le quantile de Student, on a :
$$ IC(95\%) = \mu \pm \frac{sd}{\sqrt(n)} \times qt(95\%, n-1)$$
N'oubliez pas d'adapter le choix de votre quantile aux hypothèses que valident (ou pas) vos données !


Mais ces quatre derniers graphiques restent moins informatifs qu'un graphique qui conserve encore les observations les plus brutes possibles -- surtout lorsque `n` est faible.
En effet, les trois premiers contiennent des calculs réduisant l'information des données brutes.
De plus, les densités et les diagrammes en boîtes et en bâtons ne montrent pas par défaut la taille du groupe d'échantillonnage, qu'il est (malheureusement) courant d'omettre.
Par ailleurs, la distribution sous-jacente des données est cachée dans les diagrammes en boîtes et en bâtons. 
Comme l'histogramme n'est pas la représentation la plus aisée à lire au premier regard, vous pouvez utiliser `geom_jitter()` pour un petit nombre de points et  `geom_violin()` pour un plus grand nombre de points.  

Ci-dessous une proposition d'ajout des barres d'erreur dans un graphique conservant l'information des données brutes :


C'est donc ce que je vous recommande de faire à chaque fois que vous désirerez manger des cookies !  

Comme le jeu de données est simulé (vos données dans vos rêves !), les intervalles de confiance ne se recoupent pas.
C'est un premier indicateur sympathique pour avancer que les moyennes sont statistiquement différentes avec un seuil de 5%. 

Cela ne vous empêchera pas, bien entendu, de faire un modèle inférentiel pour vérifier plus rigoureusement ! Je rappelle à tout bon entendeur que nous avons formulé implicitement une hypothèse forte d'indépendance des données : pour prouver que les deux moyennes sont bien différentes, il faudrait vérifier s'il y a un effet `Cookie`.


Il existe tout de même un moyen de faire des barres d'erreur sans hypothèse de départ sur la distribution des observations, c'est le **bootstrap**.
Sans rentrer dans les détails, c'est une méthode de rééchantillonnage non paramétrique donc moins puissante (il faudra davantage d'obervations pour détecter une différence qu'avec une méthode paramétrique).
En terme de code, avec **ggplot2**, il suffit de changer la fonction de calcul des erreurs :



## Remerciements

- Spéciale dédicace à Christel Michel qui m'a fait prendre conscience à quel point il était urgent que je créée ce document. Merci Christel 😉 !  
- Je remercie Tom Doizy pour l'illustration 3D de cookies trop la classe.  
- Merci à mes fidèles relecteurs : Ismaël Houillon et Paola Campos.

## Licence

Ce travail a été écrit avec Rmarkdown et il est mis à disposition selon les termes de la [Licence Creative Commons Attribution - Partage dans les Mêmes Conditions 4.0](https://creativecommons.org/licenses/by-sa/4.0/).


## Bibliographie









