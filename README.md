## Modèle de mail pour lancer la formation

Voici une proposition de date et une proposition de communication pour
une nouvelle session de la formation :

---

**Reproductibilité scientifique sous R avec Rmarkdown et RStudio**

**Date et lieu :  
**Mercredi 5 et jeudi 6 juillet 2023 de 8h30 à 16h

Pôle de Protection des Plantes, CIRAD, Saint-Pierre ou CIRAD Saint Denis
selon la majorité des inscrits

**Cible :**

Cette formation est à destination de tout utilisateur de R (de débutant
à avancé).

Vous travaillerez sur vos propres données. Les activités de groupes et
l’intelligence collective seront privilégiées !

**Contexte :**

L’un des principaux risques dans le cadre de la diffusion des résultats
des analyses est l’erreur liée à la transition entre R et les outils de
traitement de texte classiques (les fameux “copier-coller”). L’enjeu de
cette formation est

-   de vous fournir un outil et une méthodologie pour vous permettre
    d’optimiser la rédaction de vos documents (manuscrits de thèse,
    rapports et articles) et de gagner beaucoup de temps,

-   le cas échant, de vous permettre de transmettre à votre tour ces
    compétences à vos étudiants.

**Objectifs de la formation** :

-   Comprendre la notion de reproductibilité (jour 1 matin)
-   Utiliser Rmarkdown pour faire un premier rapport automatisé (jour 1
    après-midi)
-   Maîtriser des fonctionnalités liées à la création de fichiers
    Rmarkdown dans un environnement de développement adapté : RStudio
    (jour 2 matin)
-   Être autonome dans sa propre découverte de l’univers de Rmarkdown
    (jour 2 après-midi)

**Prérequis (sur validation de la formatrice)** :

-   Avoir un ordinateur portable

-   Avoir R, Rtools (pour les utilisateurs de Windows) et RStudio à jour

-   Avoir un projet R contenant :

    -   Un fichier script (.R) de moins de 100 lignes et qui produit au
        moins un graphique,

    -   un fichier texte illustrant le script

    -   au moins une image (.png, .jpeg, …)

    -   au moins un article de référence (au mieux sous format .bib)

**Formatrice :**

Anna Doizy (DoAna - Statistiques Réunion)

---

Joindre :

-   scénario pédagogique

-   guide de création d’une formation collective du CIRAD

## Modèle de mail pour les prérequis

Bonjour {prénom},

Merci pour ton inscription :)

Voici les instructions pour les prérequis de la formation “La
reproductibilité scientifique sous R avec Rmarkdown et RStudio”.

Un des objectifs est de construire un rapport avec tes propres données
et de tester les différentes possibilités de rmarkdown directement
dessus.

Pourrais-tu s’il te plaît m’envoyer **avant le XXX** le dossier d’un
**projet R** (en .zip) contenant :

-   Un fichier **script** (.R) de moins de 100 lignes et qui produit
    **au moins un graphique**. C’est un projet passé ou en cours, mais
    ce n’est vraiment pas important. L’essentiel, c’est qu’il soit
    court, qu’il fasse un plot et qu’il fonctionne.

-   un fichier **texte** illustrant le script. C’est la “déco” autour du
    script. Que veux-tu que ce script raconte ? Pareil, ce n’est pas
    important d’y passer beaucoup de temps, l’essentiel est de saisir le
    principe de la méthodo.

-   au moins une **image** (.png, .jpeg, …), n’importe quelle
    illustration, ça peut être un logo ou une photo, par exemple. 

-   au moins un **article de référence** (au mieux sous format .bib).
    C’est pour apprendre à insérer une référence bibliographique dans le
    rapport. Si tu connais le format .bib, tant mieux. Sinon, juste le
    texte pour citer l’article conviendra.

-   N’oublie pas de vérifier que R, Rtools (si tu utilises Windows) et
    RStudio sont à jour !

Je te mets l’exemple sur lequel on travaillera ensemble en pièce-jointe.

De mon côté, pour valider ton inscription auprès du CIRAD, j’ai besoin
de vérifier que tu sais ce qu’est un Rproject et que ton code est bien
fonctionnel, car ce n’est pas une formation de programmation avec R.

N’hésite pas à me contacter si tu as des questions.

Bonne journée,

## Matériel pour le jour J

-   cadre de sécurité

-   cartes émotions + paréo

-   jeu des ravitos

-   feutres, post it, scotch, pinces, aimants, veleda, effaceur, pate à
    fix, élastiques, gommettes

-   paperboard

-   dossier des cheatsheets R

-   cheatsheet rmarkdown pour les participants

-   ecocup DoAna

-   feuilles d’émargement + déroulé

-   ordinateur + souris + connexion internet

-   jus de fruits + bonbons (J2)

## A envoyer à l’OF à la fin

-   feuille d’émargement,

-   les attentes,

-   l’autoévaluation sur chacun des objectifs

-   les avis à chaud sur la journée

-   photos de la journée

-   facture

## Modèle de mail pour la fin

Bonjour à toutes et tous,

Je vous remercie très fort pour votre présence hier, pour votre écoute,
votre bonne humeur et votre participation active !

Je suis ravie de sentir que cette formation vous a apporté quelque chose
à toutes et tous.

Merci aussi pour vos retours enthousiastes que je vais pouvoir utiliser
pour améliorer la formation. J’ai bien noté que …

Voici le lien vers les **supports de la formation** et les ressources
complémentaires pour aller plus loin : <https://rmarkdown.doana-r.com/>

Je reste disponible pour répondre à vos questions au moment où vous vous
mettrez à votre “plus petit pas possible”. N’hésitez pas à vous
**entraider**.

Je vous prie de répondre au questionnaire du CIRAD pour leur donner
**votre avis sur la formation** :
<https://forms.office.com/Pages/ResponsePage.aspx?id=kF-vYLTj1keMjubW0rEXuaSFVo9SSvVNsZEpt7bGsGFUNDJNOU04RFZZN1g4MVRHSk1FOVEwM0NBOC4u>

Je vous mets quelques photos souvenir en PJ. J’en diffuserai
quelques-unes sur les réseaux, en masquant les visages. Dites-moi si
cela vous pose problème.

Le service formation du CIRAD (en copie du mail) reviendra vers vous
pour les **attestations de formation**.

Je vous souhaite une bonne exploration !

------------------------------------------------------------------------

Pour une nouvelle session, il faut au moins un salarié CIRAD.

OPCO lent, il faut s’y prendre un mois à l’avance.
